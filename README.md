# K8S Commands and Documentaion for CKA Exam

k8s documentation:  
https://kubernetes.io/docs/home/

# K8S Cheat sheet

https://kubernetes.io/docs/reference/kubectl/cheatsheet/

## Versioning yaml files

| Kind       | Version |
| ---------- | ------- |
| POD        | v1      |
| Service    | v1      |
| ConfigMap  | v1      |
| Secret     | v1      |
| ReplicaSet | apps/v1 |
| Deployment | apps/v1 |
| DaemonSet  | apps/v1 |

## Imperative Commands

```
kubectl run --generator=run-pod/v1 redis --image=redis:alpine --labels="tier=db"  # Create Pod with labels
kubectl replace --force -f nginx.yaml # delete and replace the pod with new definition
kubectl expose pod redis --port=6379 --name redis-service --dry-run -o yaml  # Create a Service named redis-service of type ClusterIP to expose pod redis on port 6379
kubectl create service clusterip redis --tcp=6379:6379 --dry-run -o yaml  # (This will not use the pods labels as selectors, instead it will assume selectors as app=redis. You cannot pass in selectors as an option. So it does not work very well if your pod has a different label set. So generate the file and modify the selectors before creating the service)
kubectl expose pod nginx --port=80 --name nginx-service --dry-run -o yaml # (This will automatically use the pod's labels as selectors, but you cannot specify the node port. You have to generate a definition file and then add the node port in manually before creating the service with the pod.)
kubectl create service nodeport nginx --tcp=80:80 --node-port=30080 --dry-run -o yaml # (This will not use the pods labels as selectors)
kubectl exec -it app -n namespaceName -- cat /log/app.log ### Exec to container
kubectl explain deployment | grep VERSION # Shows the version of an object
kubectl api-resources # Show all api components with versions
kubectl api-resources --namespaced=true # Show all api with namespace level
```

#### Create Deployment and Scale to 3 replicas

```
kubectl create deployment webapp --image=nginx
kubectl scale deployment webapp --replicas=3
```

---

## Pods

```
kubectl run nginx --image nginx
kubectl run httpd --image=httpd:alpine --port=80 --expose # run pod httpd and expose with clusterIp on port 80
kubectl get pods
kubectl run --generator=run-pod/v1 nginx --image=nginx  # Create nginx pod
kubectl run --generator=run-pod/v1 nginx --image=nginx --dry-run -o yaml # Generate POD Manifest YAML file (-o yaml). Don't create it(--dry-run)
```

---

## ReplicaSets

```
kubectl create -f replicaset-definition.yml
kubectl get replicaset
kubectl delete replicaset myapp-replicaset
kubectl replace -f replicaset-defintion.yml
kubectl scale --replicas=6 -f replicaset-defintion.yml
kubectl scale rs rsName --replicas=6
```

---

## Deployments

```
kubectl create -f deployment-definition.yml
kubectl get deployments
kubectl delete deployment deploymentName
kubectl create deployment --image=nginx nginx # Create a deployment
kubectl create deployment --image=nginx nginx --dry-run -o yaml # Generate Deployment YAML file (-o yaml). Don't create it(--dry-run)
kubectl create deployment --image=nginx nginx --dry-run -o yaml > nginx-deployment.yaml # Generate Deployment YAML file (-o yaml). Don't create it(--dry-run) with 4 Replicas (--replicas=4)
kubectl rollout restart deployment [depl_name]  # Restart the deployment rollout
# available only in k8s version 1.19+
kubectl create deployment --image=nginx nginx --replicas=4 --dry-run=client -o yaml > nginx-deployment.yaml
# List Deployments Rollout history
kubectl rollout history deployment/<Deployment-Name>
# List Deployment History with revision information
kubectl rollout history deployment/my-first-deployment --reviosion=1
# Undo Deployment
kubectl rollout undo deployment/my-first-deployment
# Rollback to specific Revision
kubectl rollout undo deployment/my-first-deployment --to-revision=3
# Rolling restart
kubectl rollout restart deployment/<Deployment-Name>
```

Pause Deployment and Two Changes

```
# Pause the Deployment
kubectl rollout pause deployment/<Deployment-Name>

# Update Deployment - Application Version from v3 to v4
kubectl set image deployment/my-first-deployment kubenginx=stacksimplify/kubenginx:4.0.0 --record=true
kubectl set resources deployment/my-first-deployment -c=kubenginx --limits=cpu=20m,memory=30Mi
```

Resume Deployment

```
# Resume the Deployment
kubectl rollout resume deployment/my-first-deployment
```

---

## Namespaces

Access to service in different namespace:  
serviceName.namespaceName.svc.cluster.local

```
kubectl get pods --namespace=kube-system # See pods in namespace kube-system
kubectl create -f pod-definition.yml --namespace=dev # Create pod in dev namespace
kubectl create namespace dev # create dev namespace
kubectl get pods --all-namespace # View pods in all namespaces
kubectl config set-context $(kubectl config current-context) --namespace=dev # change the default namespace to dev namespace
```

---

## Services

#### Services Types

- NodePort : Range 30000 - 32767

```
kubectl expose deployment simple-webapp-deployment --name=webapp-service --target-port=8080 --type=NodePort --port=8080 --dry-run=client -o yaml > svc.yaml
```

- ClusterIP : For Cluster Communication Only, between the different services, Can access by service name or cluster ip
- LoadBalancer : Makes a pod accessible from outside the cluster. This is the right way to expose a pod to the outside world.
- External Name : Redirects an in-cluster request to a CNAME url

```
kubectl create -f service-definition.yml
kubectl get services
kubectl get svc
curl http://192.168.1.2:30008
```

---

## Imperative Approach

### Create Objects

```
kubectl run --image=nginx nginx
kubectl create deployment --image=nginx nginx
kubectl expose deployment nginx --port 80
# Create nodePort , you can't specify the node port
kubectl expose pod nginx --type=NodePort --port=80 --name=nginx-service --dry-run=client -o yaml
# Create a Service named redis-service of type ClusterIP to expose pod redis on port 6379
kubectl expose pod redis --port=6379 --name redis-service --dry-run=client -o yaml
```

### Update Objects

```
kubectl edit deployment nginx
kubectl scale deployment nginx --replicas=5
kubectl set image deployment nginx nginx=nginx:1.18
```

---

## Scheduling

#### Manual Scheduling

```
curl --header "Content-Type:application/json" --request POST --data '{"apiVersion": "v1", "kind": "Binding", ...}' http://$SERVER/api/v1/namespaces/default/pods/$PODNAME/binding/
```

#### Labels and Selectors

```
kubectl get pods --selector app=App1 # Filter the pods by labels
kubectl get pods --selector "app=App1,tier=frontend" # Filter the pods by multiple labels
kubectl get pods --show-labels # Show all pods with labels in default namespace
kubectl get pods -l env=dev --no-headers | wc -l
```

#### Annotations

You can use Kubernetes annotations to attach arbitrary non-identifying metadata to objects. Clients such as tools and libraries can retrieve this metadata.

```
"metadata": {
  "annotations": {
    "key1" : "value1",
    "key2" : "value2"
  }
}

apiVersion: v1
kind: Pod
metadata:
  name: annotations-demo
  annotations:
    imageregistry: "https://hub.docker.com/"
spec:
  containers:
  - name: nginx
    image: nginx:1.7.9
    ports:
    - containerPort: 80
```

#### Taints And Tolerations

```
kubectl taint nodes node-name key=value:taint-effect
kubectl taint nodes node01 app=blue:NoSchedule
kubectl describe node kubemaster | grep Taint  # To see the taint on master node.
kubectl taint nodes master node-role.kubernetes.io/master:NoSchedule-  # Remove taint from node
kubectl describe node kubemaster | grep Taint # Show taint on master node
kubectl explain pod --recursive | less # Explaint all the options in pod
kubectl explain pod --recursive | grep -A5 tolerations # give 5 lines after the search about toleration
```

taint effect - what happens to PDOs that do not tolerate this taint ?
NoSchedule | PreferNoSchedule | NoExecute

**_Tolerations - PODS_**

```
apiVersion: v1
kind: Pod
metadata:
  name: myapp-pod
  labels:
    app: myapp
    type: front-end

spec:
  containers:
    - name: nginx-container
      image: nginx
  tolerations:
  - key: "app"
    operator: "Equal"
    value: "blue"
    effect: "NoSchedule"
```

#### Node Selector

**yml example:**

```
apiVersion: v1
kind: Pod
metadata:
  name: myapp-pod
  labels:
    app: myapp
    type: front-end

spec:
  containers:
    - name: nginx-container
      image: nginx
  nodeSelector:
    size: Large
```

**Label Node:**

```
kubectl label nodes <node-name> <label-key>=<label-value>
kubectl label nodes node-1 size=Large
```

There is an limitation of using Node Selectors And Labels

- You cant select Large Or Medium
- You cant select Not a Small

#### Node Affinity

**Node Affinity Types**  
Available:

- requiredDuringSchedulingIgnoredDuringExecution
- preferredDuringSchedulingIgnoredDuringExecution  
  Planned:
- requiredDuringSchedulingRequiredDuringExecution

**Pod definition file**

```
apiVersion: v1
kind: Pod
metadata:
  name: myapp-pod
  labels:
    app: myapp
    type: front-end

spec:
  containers:
    - name: nginx-container
      image: nginx
  affinity:
    nodeAffinity:
      requiredDuringSchedulingIgnoredDuringExecution:
        nodeSelectorTerms:
          - matchExpressions:
              - key: size
                operator: In
                values:
                  - Large
                  - Medium
```

```
apiVersion: v1
kind: Pod
metadata:
  name: myapp-pod
  labels:
    app: myapp
    type: front-end

spec:
  containers:
    - name: nginx-container
      image: nginx
  affinity:
    nodeAffinity:
      requiredDuringSchedulingIgnoredDuringExecution:
        nodeSelectorTerms:
          - matchExpressions:
              - key: size
                operator: NotIn
                values:
                  - Small
```

Check if the size key exists in the node:

```
apiVersion: v1
kind: Pod
metadata:
  name: myapp-pod
  labels:
    app: myapp
    type: front-end

spec:
  containers:
    - name: nginx-container
      image: nginx
  affinity:
    nodeAffinity:
      requiredDuringSchedulingIgnoredDuringExecution:
        nodeSelectorTerms:
          - matchExpressions:
              - key: size
                operator: Exists
```

---

## Resource Requirements and Limits

#### Resource requests

- cpu: 0.5 memory: 256 Mi
- 0.1 CPU = 100m

```
apiVersion: v1
kind: Pod
metadata:
  name: myapp-pod
  labels:
    app: myapp
    type: front-end

spec:
  containers:
    - name: nginx-container
      image: nginx
      ports:
        - containerPort: 8080
      resources:
        requests:
          memory: "1Gi"
          cpu: 1
```

#### Resource Limits

- Default cpu limit: 1vCPU
- Default memory limit: 512 Mi

```
apiVersion: v1
kind: Pod
metadata:
  name: myapp-pod
  labels:
    app: myapp
    type: front-end

spec:
  containers:
    - name: nginx-container
      image: nginx
      ports:
        - containerPort: 8080
      resources:
        requests:
          memory: "1Gi"
          cpu: 1
        limits:
          memory: "2Gi"
          cpu: 2
```

---

## DaemonSets

#### Definition file

```
apiVersion: apps/v1
kind: DaemonSet
metadata:
  name: monitoring-daemon
spec:
  selector:
    matchLabels:
      app: monitoring-agent
  template:
    metadata:
      labels:
        app: monitoring-agent
    spec:
      containers:
      - name: monitoring-agent
        image: monitoring-agent
```

---

## Static Pods

- We can only create static pods - kubelet works with pod level
- Location of yaml files: /etc/kubernetes/manifests
- The location of yaml files configured in kubelet.service - pod-manifest-path
- Use cases: Deploy Control Plane components as Static Pods

---

## Multiple Schedulers

- my-custom-scheduler.yaml

```
apiVersion: v1
kind: Pod
metadata:
  name: my-custom-scheduler
  namespace: kube-system
spec:
  containers:
  - command:
    - kube-scheduler
    - --address=127.0.0.1
    - --kubeconfig=/etc/kubernetes/sheduler.conf
    - --leader-elect=true
    - --scheduler-name=my-custom-scheduler
    - --lock-object-name=my-custom-scheduler

    image: k8s.gcr.io/kube-scheduler-amd64:v1.11.3
    name: kube-scheduler
```

- Use custom scheduler in pod definition file:

```
apiVersion: v1
kind: Pod
metadata:
  name: nginx
spec:
  containers:
  - image: nginx
    name: nginx
  schedulerName: my-custom-sheduler
```

- View events

```
kubectl get events
```

---

## Monitoring & Logging

#### Metric Server

- git clone https://github.com/kubernetes-incubator/metrics-server.git
- kubectl create -f deploy/1.8+/

```
kubectl top node
kubectl top pod
kubectl top pod -A # top pods in all namespaces
kubectl logs podName -f containerName # If there is more than 1 container in the pod, choose 1 of them to monitor
```

---

## Application Lifecycle Management

#### Rolling Updates and Rollbacks

```
kubectl rollout status deployment/myapp-deployment
kubectl rollout history deployment/myapp-deployment
kubectl rollout undo deployment/myapp-deployment ### rollback to previous revision
kubectl set image deployment/myapp-deployment nginx=nginx:1.9.1
```

---

## Commands and Arguments

- Docker file

```
FROM Ubuntu
ENTRYPOINT ["sleep"]
CMD ["5"]
```

- Pod definition file

```
apiVersion: v1
kind: Pod
metadata:
  name: ubuntu-sleeper-pod
spec:
  containers:
    - name: ubuntu-sleeper
      image: ubuntu-sleeper
      command: ["sleep2.0"] # Overwrites the entry point command from dockerfile
      args: ["10"] # Overwrites the cmd default from dockerfile
```

---

## ENV Variables

- Pod definition file

```
apiVersion: v1
kind: Pod
metadata:
  name: ubuntu-sleeper-pod
spec:
  containers:
    - name: ubuntu-sleeper
      image: ubuntu-sleeper
      env:
        - name: APP_COLOR
          value: pink
```

---

## Config Maps

```
kubectl create configmap app-config --from-literal=APP_COLOR=blue --from-literal=APP_MOD=prod # imperative
kubectl create configmap app-config --from-file=app_config.properties ### Create config map from file
kubectl get configmaps
kubectl describe configmaps
```

config--map.yaml

```
apiVersion: v1
kind: ConfigMap
metadata:
  name: app-config
data:
  APP_COLOR: blue
  APP_MODE: prod
```

pod-definition.yaml

```
apiVersion: v1
kind: Pod
metadata:
  name: simple-webapp-color
  labels:
    name: simple-webapp-color
spec:
  containers:
  - name: simple-webapp-color
    image: simple-webapp-color
    ports:
      - containerPort: 8080
    envFrom:
      - configMapRef:
          name: app-config
```

---

## Configure Secrets in Applications

```
kubectl create secret generic <secret-name> --from-literal=<key>=<value>
kubectl create secret generic app-secret --from-literal=DB_Host=mysql --from-literal=DB_Password=passwd
kubectl create sercret generic app-secret --from-file=app_secret.properties ### create secret from a file
kubectl get secrets
kubectl describe sercrets app-secret
kubectl get secret app-secret -o yaml ### to show decoded secret
```

secret-data.yaml

```
apiVersion: v1
kind: Secret
metadata:
  name: app-secret
data:
  DB_Host: bX1zcw===
  DB_User: bX1zcw===asd
  DB_Password: bX1zcw===asd
```

Convert password to base64

```
echo -n 'passwd' | base64
```

Decode the secret to plain text

```
echo -n 'bX1cWw==' | base64 --decode
```

pod-definition.yaml and refer to the secret

```
apiVersion: v1
kind: Pod
metadata:
  name: simple-webapp-color
  labels:
    name: simple-webapp-color
spec:
  containers:
  - name: simple-webapp-color
    image: simple-webapp-color
    ports:
      - containerPort: 8080
    envFrom:
      - secretRef:
          name: app-secret
```

```
apiVersion: v1
kind: Pod
metadata:
  name: simple-webapp-color
  labels:
    name: simple-webapp-color
spec:
  containers:
  - name: simple-webapp-color
    image: simple-webapp-color
    ports:
      - containerPort: 8080
    volumes:
      - name: app-secret-volume
        secret:
          secretName: app-secret
```

- ls /opt/app-secret-volumes
- cat /opt/app-secret-volumes/DB_Password

---

## Multi Container Pods

pod-definition.yaml

```
apiVersion: v1
kind: Pod
metadata:
  name: simple-webapp
  labels:
    name: simple-webapp
spec:
  containers:
  - name: simple-webapp
    image: simple-webapp
    ports:
      - containerPort: 8080
  - name: log-agent
    image: log-agent
```

#### InitContainers

```
apiVersion: v1
kind: Pod
metadata:
  name: myapp-pod
  labels:
    app: myapp
spec:
  containers:
  - name: myapp-container
    image: busybox:1.28
    command: ['sh', '-c', 'echo The app is running! && sleep 3600']
  initContainers:
  - name: init-myservice
    image: busybox
    command: ['sh', '-c', 'git clone <some-repository-that-will-be-used-by-application> ; done;']
```

Multi Init Containers:

```
apiVersion: v1
kind: Pod
metadata:
  name: myapp-pod
  labels:
    app: myapp
spec:
  containers:
  - name: myapp-container
    image: busybox:1.28
    command: ['sh', '-c', 'echo The app is running! && sleep 3600']
  initContainers:
  - name: init-myservice
    image: busybox:1.28
    command: ['sh', '-c', 'until nslookup myservice; do echo waiting for myservice; sleep 2; done;']
  - name: init-mydb
    image: busybox:1.28
    command: ['sh', '-c', 'until nslookup mydb; do echo waiting for mydb; sleep 2; done;']
```

---

## Cluster Maintenance

OS Upgrades

```
kubectl drain node-01  ### Removes all pods from the node and mark him as unschedulable
kubectl drain node01 --ignore-daemonsets ## Ignore daemon sets when draining the node
kubectl uncordon node-01 ### Return the node to scheduled
kubectl cordon node-02 ### Mark node as unschedulable but dont remove the pods from him
```

#### Cluster Upgrades

1. Upgrade master nodes
2. Upgrade Worker nodes

With kubeadm

```
apt-get upgrade -y kubeadm=1.12.0-00
kubeadm upgrade plan
kubeadm upgrade apply
kubeadm upgrade apply v1.13.4
```

Upgrade Kubelet

```
apt-get upgrade -y kubelet=1.12.0-00
systemctl restart kubelet
```

Upgrade worker nodes

```
kubectl drain node01
apt-get upgrade -y kubeadm=1.12.0-00
apt-get upgrade -y kubelet=1.12.0-00
kubeadm upgrade node config --kubelet-version v.1.12.0
systemctl restart kubelet
kubectl uncordon node01
```

#### Backup and restore

Resource

```
kubectl get all --all-namespaces -o yaml > all-deploy-services.yaml
```

We can use valero project to backup resources

ETCD Cluster  
Take snapshot of etcd database

```
export ETCDCTL_API=3
ETCDCTL_API=3 etcdctl snapshot save snapshot.db \
--endpoints=https://127.0.0.1:2379 \
--cacert=/etc/etcd/ca.crt \
--cert=/etc/etcd/etcd-server.crt \
--key=/etc/etcd/etcd-server.key
```

See etcd snapshot status

```
ETCDCTL_API=3 etcdctl snapshot status snapshot.db
```

Restore the backup

```
service kube-apiserver stop
ETCDCTL_API=3 etcdctl snapshot restore snapshot.db --data-dir /var/lib/etcd-from-backup
```

in etcd.service change

```
--data-dir=/var/lib/etcd-from-backup
```

```
systemctl daemon-reload
service etcd restart
service kube-apiserver start
```

---

## Security

#### Authentication

Who can access the cluster?

- Files - Username and Passwords
- Files - Username and Tokens
- Certificates
- External Authentication providers - LDAP
- Service Accounts

What can they do?

- RBAC Authorization
- ABAC Authorization
- Node Authorization
- Webhook Mode

Auth Mechanisms for kube-apiserver

- Static password file
- Static Token file
- Certificates
- Identity Services

#### TLS in kubernetes - Certificate Creation

**Certificate (Public Key)**

- _.crt _.pem
- server.crt
- server.pem
- client.crt
- client.pem

**Private Key**

- _.key _-key.pem
- server.key
- server-key.pem
- client.key
- client-key.pem

**CERTIFICATE AUTHORITY**

```
openssl genrsa -out ca.key 2048 ### Generate Keys ca.key
openssl req -new -key ca.ley -subj "/CN=KUBERNETES-CA" -out ca.csr ### Certificate Signing Request
openssl x509 -req -in ca.csr -signkey ca.key -out ca.crt ### Sign Certificates

```

**ADMIN USER**

```
openssl genrsa -out admin.key 2048 ### Generate keys
openssl req -new -key admin.key -subj "/CN=kube-admin/O=system:masters" -out admin.csr ### Certificate Signing Request
openssl x509 -req -in admin.csr -CA ca.crt -CAKEY ca.key -out admin.csr ### Sign Certificates
```

Access To kube-apiserver

```
curl https://kube-apiserver:6443/api/v1/pods \
--key admin.key --cert admin.crt --cacert ca.crt
```

kube-config.yaml

```
apiVersion: v1
clusters:
- cluster:
    certificate-authority: ca.crt
    server: https://kube-apiserver:6443
  name: kubernetes
kind: Config
users:
- name: kubernetes-admin
  user:
    client-certificate: admin.crt
    client-key: admin.key
```

Decode Certificate

```
openssl x509 -in /etc/kubernetes/pki/apiserver.crt -text -noout
```

Certificate logs

```
journalctl -u etcd.service -l
kubectl logs etcd-master
docker ps -a
docker logs 87fc
```

#### Certificate API

All the operations in kubernetes with certificates done by controller manager

1. Create CertificateSigningRequest Object

```
openssl genrsa -out jane.key 2048 ### jane.key
openssl req -new -key jane.key -subj "/CN=jane" -out jane.csr ###
cat jane.csr | base64 | tr -d "\n"
```

2. Review Requests
   jane-csr.yaml

```
apiVersion: certificates.k8s.io/v1
kind: CertificatesSigningRequest
metadata:
  name: jane
spec:
  groups:
  - system:authenticated
  signerName: kubernetes.io/kube-apiserver-client
  usages:
  - client auth
  - digital signature
  - key encipherment
  - server auth
  request:
    LS0tLS1CRUdJTiBDRVJUSUZJQ0FURSBSRVFVRVNULS0tLS0KTUlJQ1ZEQ0NBVHdDQVFBd0R6RU5N
    QXNHQTFVRUF3d0VhbUZ1WlRDQ0FTSXdEUVlKS29aSWh2Y05BUUVCQlFBRApnZ0VQQURDQ0FRb0Nn
    Z0VCQUtZUlFPR2VXdVpSWlk0SW9KdXlab3BYMmE4TmxHeGV2Q2l4NnE4L1pCT3QzZEQ3CjNrdTA2
    TDlOUkcrQ3JHdUpqZzdRdTc4K3Zob2RmdEdmMGludGZxVEUyTE5oTXZYcHJLajN5RXVLOGhQeGl3
    UmgKZEdpZVFLaTFwYitidFJxejdaSHVScG9jNnZJV2hpUVJWYU84a0cwekFSQUJjcW1sSVp6eE5Y
    VXh5TWlUV3NHMApiUktiSjk1MDc2U3p1U2VCSkhjVDJraHY5OVpRaDkwQVpqc0dudnl0dzZ0blFr
    NmFEL2p1S2RDbFdPeU1Eb2lVCkZuajBFcEw1U3lXYzJXbkRtM21ORDdLSkcxR25TSll1NEcrVlU4
    Z24vS2ptVUdZY3htbjVWRFA5NmlCcUtLbzQKTXcyQVFKeXd0QWs2dnlwU0t2TkdOTTZ0TW1IVnYx
    MDJnMDZVYmI4Q0F3RUFBYUFBTUEwR0NTcUdTSWIzRFFFQgpDd1VBQTRJQkFRQnNQbXdOVXhkZnZN
    TkQgQ0VSVElGSUNBVEUgUkVRVUVTVC0tLS0tCg==
```

3. Approve Requests

```
kubectl get csr
kubectl certificate approve jane
```

4. Share Certs to Users

```
kubectl get csr jane -o yaml
echo "LS0....=" | base64 --decode
```

curl the api server with certificates

```
curl https://my-kube-playground:6443/api/v1/pods \
--key admin.key
--cert admin.crt
--cacert ca.crt
```

with kubectl command and certificates

```
kubectl get pods
  --server my-kube-playgroud:6443
  --client-key admin.key
  --client-certificate admin.crt
  --certificate-authority ca.crt
```

#### KubeConfig

config file destination:  
$HOME/.kube/config

KubeConfig File:

```
apiVersion: v1
kind: Config

current-context: my-kube-playground

clusters:

- name: my-kube-playground
  cluster:
    certificate-authority: /etc/kubernetes/pki/ca.crt OR
    certificate-authority-data: LS0tLS1CRUdJTiBDRVJUSUZJQ0FURSBSRVFVRVNULS0tLS0KTUlJQ1ZEQ0NBVHdDQVFBd0R6RU5N
                QXNHQTFVRUF3d0VhbUZ1WlRDQ0FTSXdEUVlKS29aSWh2Y05BUUVCQlFBRApnZ0VQQURDQ0FRb0Nn
                Z0VCQUtZUlFPR2VXdVpSWlk0SW9KdXlab3BYMmE4TmxHeGV2Q2l4NnE4L1pCT3QzZEQ3CjNrdTA2
                TDlOUkcrQ3JHdUpqZzdRdTc4K3Zob2RmdEdmMGludGZxVEUyTE5oTXZYcHJLajN5RXVLOGhQeGl3
                UmgKZEdpZVFLaTFwYitidFJxejdaSHVScG9jNnZJV2hpUVJWYU84a0cwekFSQUJjcW1sSVp6eE5Y
    server: https://my-kube-playground:6443
- name: google
   .......

contexts:

- name: my-kube-admin@my-kube-playground
  context:
    cluster: my-kube-playground
    user: my-kube-admin
- name: dev-user@google
  context:
    cluster: google
    user: dev-user
    namespace: finance

users:

- name: my-kube-admin
  user:
    client-certificate: admin.crt
    client-key: admin.key
- name: dev-user
  user:
    client-certificate: admin.crt
    client-key: admin.key
```

```
kubectl config view
kubectl config view --kubeconfig=my-custom-config
kubectl config use-context prod-user@production ### Change context
kubectl config --kubeconfig=/root/my-kube-config use-context contextName ### change context with different config file
kubectl config -h
```

---

## RBAC - Role Based Acceess Control

### Role and Rolebinding

Namespaced scope:

- pods
- replicasets
- jobs
- deployments
- services
- secrets
- roles
- rolebinding
- configmaps
- PVC

developer-role.yaml

- Can view PODS
- Can create PODS
- Can delete PODS
- Can Create ConfigMaps

```
apiVersion: rbac.authorization.k8s.io/v1
kind: Role
metadata:
  name: developer
rules:
- apiGroups: [""]
  resources: ["pods"]
  verbs: ["list", "get", "create", "update", "delete"]
- apiGroups: [""]
  resources: ["ConfigMap"]
  verbs: ["create"]
```

```
kubectl create -f developer-role.yaml
```

Next step is to link user to the role  
devuser-developer-binding.yaml

```
apiVersion: rbac.authorization.k8s.io/v1
kind: RoleBinding
metadata:
  name: devuser-developer-binding
subjects:
- kind: User
  name: dev-user
  apiGroup: rbac.authorization.k8s.io
roleRef:
  kind: Role
  name: developer
  apiGroup: rbac.authorization.k8s.io
```

```
kubectl create -f devuser-developer-binding.yaml
kubectl get roles ### to view the roles
kubectl get rolebindings ### to view role bindings
kubectl describe role developer ### show more details about the role
kubectl describe rolebinding devuser-developer-binding ### show more details about rolebinding
```

**Check Access**

```
kubectl auth can-i create deployments ### yes
kubectl auth can-i delete nodes ### no
kubectl auth can-i create deployments --as dev-user ### check access for another user
kubectl auth can-i create pods --as dev-user
kubectl auth can-i create pods --as dev-user --namespace test ### check if dev-user can create pods in test namespace
```

```
kubectl api-resources --namespaces=true
```

### Cluster Roles and Role Bindings

Cluster Scoped:

- nodes
- PV
- clusterroles
- clusterrolebindings
- certificatesigningrequests
- namespaces

```
kubectl api-resources --namespaces=false
```

cluster-admin-role.yaml

```
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRole
metadata:
  name: cluster-administrator
rules:
- apiGroups: [""]
  resources: ["nodes"]
  verbs: ["list", "get", "create", "delete"]
```

link user to the role - cluster-admin-role-binding.yaml

```
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  name: cluster-admin-role-binding
subjects:
- kind: User
  name: cluster-admin
  apiGroup: rbac.authorization.k8s.io
roleRef:
  kind: ClusterRole
  name: cluster-administrator
  apiGroup: rbac.authorization.k8s.io
```

### Service Accounts

Example for using service accounts:

- Prometheus
- Jenkins
- Dynatrace

To create service account:

```
kubectl create serviceaccount dashboard-sa
```

Automatically create a token for the service account and store it as secret object  
To view the token :

```
kubectl describe secret dashboard-sa-token-kbbdm
```

pod-definition.yaml

```
apiVersion: v1
kind: Pod
metadata:
  name: my-kubernetes-dashboard
spec:
  containers:
    - name: my-kubernetes-dashboard
      image: my-kubernetes-dashboard
  serviceAccountName: dashboard-sa
```

### Image Security

```
docker login private-registry.io # Login to private registry
kubectl create secret docker-registry regcred --docker-server=serverName --docker-username=userName --docker-password=dockerPassword --docker-email=DockerEmail
```

Use the secret in Pod / Deployment:

```
apiVersion: v1
kind: Pod
metadata:
  name: my-kubernetes-dashboard
spec:
  containers:
    - name: my-kubernetes-dashboard
      image: my-kubernetes-dashboard
  imagePullSecrets:
  - name: regcred
```

### Security Context

Run in pod level:

```
apiVersion: v1
kind: Pod
metadata:
  name: web-pod
spec:
  securityContext:
    runAsUser: 1000
  containers:
    - name: ubuntu
      image: ubuntu
      command: ["sleep", "3600"]
```

Run in container level:

```
apiVersion: v1
kind: Pod
metadata:
  name: web-pod
spec:
  containers:
    - name: ubuntu
      image: ubuntu
      command: ["sleep", "3600"]
      securityContext:
        runAsUser: 1000
        capabilities:
          add: ["MAC_ADMIN"] # Capabilities are only supported at the container level and not at the pod level
```

### Network Policies

Policy to allow connection only from API Pod to DB Pod and from backup server:

```
apiVersion: networking.k8s.io/v1
kind: NetworkPolicy
metadata:
  name: db-policy
spec:
  podSelector:
    matchLabels:
      role: db
  policyTypes:
  - Ingress
  ingress:
  - from:
    - podSelector:
        matchLabels:
          name: api-pod
      namespaceSelector:
        matchLabels:
          name: prod
    - ipBlock:
        cidr: 192.168.5.10/32
    ports:
    - protocol: TCP
      port: 3306
```

Notes:  
Solutions that support network policies:

- kube-router
- Calico
- Romana
- Weave-net

Solutions that DO NOT support network policies:

- Flannel

---

## Storage

### Storage in Docker

File System:

```
/var/lib/docker
..aufs
..containers
..image
..volumes
```

Dockeer volumes:

```
docker volume create data_volume
```

Creates volumes folder:

```
/var/lib/docker
..volumes
.....data_volume
```

Mount the volume:

```
docker run -v data_volume:/var/lib/mysql mysql
```

Mount custom folder /data/mysql from our host to /var/lib/mysql in container:

```
docker run -v /data/mysql:/var/lib/mysql mysql
```

Another option to mount:

```
docker run --mount type=bind,source=/data/mysql,targe=/var/lib/mysql mysql
```

Storage Drivers:

- AUFS
- ZFS
- BTRFS
- Device Mapper
- Overlay
- Overlay2

Volume Drivers:

- Local
- Azure File Storage
- Convoy
- DigitalOcean Block Storage
- Flocker
- gce-docker
- GlusterFS
- NetApp
- RexRay
- Portworx
- VMware vSphere Storage
- etc...

Run docker with specific volume driver:

```
docker run -it --name mysql --volume-driver rexray/ebs --mount src=ebs-vol, target=/var/lib/mysql mysql
```

### Container Storage Interface

Some Standards:

- Container Runtime Interface - **CRI** - Examples: rkt, docker, cri-o
- Container Network Interface - **CNI** - Examples: weaveworks, flannel, cilium
- Container Storage Interface - **CSI** - Examples: portworx, Anmazon EBS, Dell EMC, GlusterFS

CSI - RPC :

- Should call to provision a new volume
- Should call to delete a volume
- Should call to place a workload that uses the volume onto a node
- Should provision a new volume on the storage
- Should decommission a volume
- Should make the volume available on a node

### Volumes

Volumes & Mounts:
Mount path to the node:

```
apiVersion: v1
kind: Pod
metadata:
  name: random-number-generator
spec:
  containers:
  - image: alpine
    name: alpine
    command: ["/bin/sh", "-c"]
    args: ["shuf -i 0-100 -n 1 >> /opt/number.out;"]
    volumeMounts:
    - mountPath:
      name: data-volume
  volumes:
  - name: data-volume
    hostPath:
      path: /data
      type: Directory
```

### Persistent Volumes

Central Manage storage solution

pv-defintion.yaml

```
apiVersion: v1
kind: PersistentVolume
metadata:
  name: pv-voll
spec:
  accessModes:
    - ReadWriteOnce / ReadOnlyMany / ReadWriteMany
  capacity:
    storage: 1Gi
  hostPath:
    path: /tmp/data
```

pv-defintion-aws.yaml

```
apiVersion: v1
kind: PersistentVolume
metadata:
  name: pv-voll
spec:
  accessModes:
    - ReadWriteOnce / ReadOnlyMany / ReadWriteMany
  capacity:
    storage: 1Gi
  awsElasticBlockStore:
    volumeID: <volumeID>
    fsType: ext4
```

```
kubectl create -f pv-defintion.yaml
kubectl get persistentvolume
```

### Persistent Volume Claims

pvc-defintion.yaml

```
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: myclaim
spec:
  accessModes:
    - ReadWriteOnce
  resources:
    requests:
      storage: 500Mi
```

```
kubectl create -f pvc-defintion.yaml
kubectl get persistentvolumeclaim
kubectl delete persistentvolumeclaim myclaim
```

Once you create a PVC use it in a POD definition file by specifying the PVC Claim name under persistentVolumeClaim section in the volumes section like this:

```
apiVersion: v1
kind: Pod
metadata:
  name: mypod
spec:
  containers:
    - name: myfrontend
      image: nginx
      volumeMounts:
      - mountPath: "/var/www/html"
        name: mypd
  volumes:
    - name: mypd
      persistentVolumeClaim:
        claimName: myclaim
```

The same is true for ReplicaSets or Deployments. Add this to the pod template section of a Deployment on ReplicaSet.

### Storage Classes

Dynamic Provisioning - The pv creates automatically, we don't need to create it.

sc-definition.yaml

```
apiVersion: storage.k8s.io/v1
kind: StorageClass
metadata:
  name: google-storage
provisioner: kubernetes.io/gce-pd
parameters:
  type: pd-standard
  replication-type: none
```

pvc-definition.yaml

```
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: myclaim
spec:
  accessModes:
    - ReadWriteOnce
  storageClassName: google-storage
  resources:
    requests:
      storage: 500Mi
```

pod-definition.yaml

```
apiVersion: v1
kind: Pod
metadata:
  name: mypod
spec:
  containers:
    - name: myfrontend
      image: nginx
      volumeMounts:
      - mountPath: "/var/www/html"
        name: mypd
  volumes:
    - name: mypd
      persistentVolumeClaim:
        claimName: myclaim
```

## Networking

### Docker Networking

Host:

```
docker run --network host nginx
```

Bridge:

```
docker run nginx
```

```
docker network ls
```

- Each container in bridge network connected to the bridge docker0
- To access the container in bridge network we need to create a port mapping :

```
docker run -p 8080:80 nginx
```

- Docker uses iptables to forward the port from 8080 to port 80 nginx
- See the rules in iptables:

```
iptables -nvL -t nat
```

### CNI - Container Networking Interface

- Container Runtime must create network namespace
- Identity network the container must attach to
- Container Runtime to invoke Network Plugin ( bridge ) when container is Added
- Container Runtime to invoke Network Plugin ( bridge ) when container id Deleted
- JSON format of the network configuration

- Must support command line arguments ADD/DEL/CHECK
- Must support parameters container id, network ns etc...
- Must manage IP Address assignment to PODs
- Must return results in a specific format

### Networking Commands to explore the network

```
ip link
ip addr
ip addr add 192.168.1.10/24 dev eth0
ip route
ip route add 192.168.1.0/24 via 192.168.2.1
cat /proc/sys/net/ipv4/ip_forward
arp
netstat -plnt
route
```

### Addons

- https://kubernetes.io/docs/concepts/cluster-administration/addons/
- https://kubernetes.io/docs/concepts/cluster-administration/networking/#how-to-implement-the-kubernetes-networking-model
- https://v1-22.docs.kubernetes.io/docs/setup/production-environment/tools/kubeadm/high-availability/#steps-for-the-first-control-plane-node

### POD Networking

- Every pod should have an IP Address.
- Every pod should be able to communicate with every other pod in the same node.
- Every pod should be able to communicate with every other pod on other nodes without NAT.

### Service Networking

```
kube-api-server --service-cluster-ip-range ipNet (default: 10.0.0.0/24)
ps aux | grep kube-api-server  # can see under range= 10.96.0.0/12
ipcalc -b networkRange
```

### DNS in kubernetes

- Kubernetes deploys a build in dns server when we creating a cluster.
- Access service - http://web-service.namespace.svc.cluster.local

### Core DNS

- deployed as pod in kube-system namespace

```
cat /etc/coredns/Corefile
kubectl get configmap -n kube-system # coredns configmap
kubectl get service -n kube-system # kube-dns ClusterIP service
```

### INGRESS

- Deploy: Ingress Controller
- Configure: Ingress Resources

#### Ingress Controller

- GCP HTTP(S) Load Balancer (GCE)
- Ngin
- Contour
- HAPROXY
- traefik
- Istio

#### Nginx Ingress Controller

Deployment:

```
apiVersion: extensions/v1beta1
kind: Deployment
metadata:
  name: nginx-ingress-controller
spec:
  replicas: 1
  selector:
    matchLabels:
      name: nginx-ingress
  template:
    metadata:
      labels:
        name: nginx-ingress
    spec:
      containers:
        - name: nginx-ingress-controller
          image: quay.io/kubernetes-ingress-controller/nginx-ingress-controller:0.21.0
      args:
        - /nginx-ingress-controller
        - --configmap=$(POD_NAMESPACE)/nginx-configuration
      env:
        - name: POD_NAME
          valueFrom:
            fieldRef:
              fieldPath: metadata.name
        - name: POD_NAMESPACE
          valueFrom:
            fieldRef:
              fieldPath:: metadata.namespace
      ports:
        - name: http
          containerPort: 80
        - name: https
          containerPort: 443
```

ConfigMap:

```
apiVersion: v1
kind: ConfigMap
metadata:
  name: nginx-configuration
```

Service:

```
apiVersion: v1
kind: Service
metadata:
  name: nginx-ingress
spec:
  type: NodePort
  ports:
    - port: 80
      targetPort: 80
      protocol: TCP
      name: http
    - port: 443
      targetPort: 443
      protocol: TCP
      name: https
  selector:
    name: nginx-ingress
```

Service Account:

```
apiVersion: v1
kind: ServiceAccount
metadata:
  name: nginx-ingress-serviceaccount
```

#### Ingress Resource

ingress-wear.yaml

```
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
 name: ingress-wear
spec:
  backend:
    serviceName: wear-service
    servicePort: 80
```

ingress-wear-watch.yaml  
my-online-store.com/wear  
my-online-store.com/watch

```
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: ingress-wear-watch
spec:
  rules:
  - http:
      paths:
      - path: /wear
        pathType: Prefix
        backend:
          service:
            name: wear-service
            port:
              number: 80

      - path: /watch
        pathType: Prefix
        backend:
          service:
            name: watch-service
            port:
              number: 80
```

```
kubectl describe ingress ingress-wear-watch
```

2 rules for:  
wear.my-online-store.com  
watch.my-online-store.com

```
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: ingress-wear-watch
spec:
  rules:
  - host: wear.my-online-store.com
    http:
      paths:
      - backend:
          service:
            name: wear-service
            port:
              number: 80
  - host: watch.my-online-store.com
    http:
      paths:
      - backend:
          service:
            name: watch-service
            port:
              number: 80
```

imperative way to create ingress:

```
kubectl create ingress <ingress-name> --rule="host/path=service:port"
kubectl create ingress ingress-test --rule="wear.my-online-store.com/wear*=wear-service:80"
```

### Ingress - Annotations and rewrite-target

Different ingress controllers have different options that can be used to customise the way it works. NGINX Ingress controller has many options that can be seen here. I would like to explain one such option that we will use in our labs. The Rewrite target option.

Our watch app displays the video streaming webpage at

```
http://<watch-service>:<port>/
```

Our wear app displays the apparel webpage at

```
http://<wear-service>:<port>/
```

We must configure Ingress to achieve the below. When user visits the URL on the left, his request should be forwarded internally to the URL on the right. Note that the /watch and /wear URL path are what we configure on the ingress controller so we can forwarded users to the appropriate application in the backend. The applications don't have this URL/Path configured on them:

```
http://<ingress-service>:<ingress-port>/watch --> http://<watch-service>:<port>/
```

```
http://<ingress-service>:<ingress-port>/wear --> http://<wear-service>:<port>/
```

Without the rewrite-target option, this is what would happen:

```
http://<ingress-service>:<ingress-port>/watch --> http://<watch-service>:<port>/watch
http://<ingress-service>:<ingress-port>/wear --> http://<wear-service>:<port>/wear
```

Notice watch and wear at the end of the target URLs. The target applications are not configured with /watch or /wear paths. They are different applications built specifically for their purpose, so they don't expect /watch or /wear in the URLs. And as such the requests would fail and throw a 404 not found error.

To fix that we want to "ReWrite" the URL when the request is passed on to the watch or wear applications. We don't want to pass in the same path that user typed in. So we specify the rewrite-target option. This rewrites the URL by replacing whatever is under rules->http->paths->path which happens to be /pay in this case with the value in rewrite-target. This works just like a search and replace function.

For example: replace(path, rewrite-target)
In our case: replace("/path","/")

```
apiVersion: extensions/v1beta1
kind: Ingress
metadata:
  name: test-ingress
  namespace: critical-space
  annotations:
    nginx.ingress.kubernetes.io/rewrite-target: /
spec:
  rules:
  - http:
      paths:
      - path: /pay
        backend:
          serviceName: pay-service
          servicePort: 8282
```

In another example given here, this could also be: replace("/something(/|$)(.\*)", "/$2")

```
apiVersion: extensions/v1beta1
kind: Ingress
metadata:
  annotations:
    nginx.ingress.kubernetes.io/rewrite-target: /$2
  name: rewrite
  namespace: default
spec:
  rules:
  - host: rewrite.bar.com
    http:
      paths:
      - backend:
          serviceName: http-svc
          servicePort: 80
        path: /something(/|$)(.*)
```

---

## Troubleshooting

### Application Failures

```
kubectl get nodes
kubectl get pods
kubectl get pods -n kube-system # Check Controlplane Pods

# Check Controlplane Services
service kube-apiserver status
service kube-controller-manager status
service kube-scheduler status

# Check Controlplane Services on worker nodes
service kubelet status
service kube-proxy status

# Check Service Logs
kubectl logs kube-apiserver-master -n kube-system
sudo journalctl -u kube-apiserver
```

### Worker node failures

```
kubectl get nodes # Check node status
kubectl describe node worker-1
top # CPU
df -h  # Disk space
service kubelet status # Check Kubelet Status
sudo journalctl -u kubelet
openssl x509 -in /var/lib/kubelet/worker-1.crt -text # Check certificates
```

### Network troubleshooting

1. Weave Net install
   Note: Weave does support kubernetes network policies.

```
kubectl apply -f "https://cloud.weave.works/k8s/net?k8s-version=$(kubectl version | base64 | tr -d '\n')"
```

2. Flannel

```
kubectl apply -f https://raw.githubusercontent.com/coreos/flannel/2140ac876ef134e0ed5af15c65e414cf26827915/Documentation/kube-flannel.yml
```

3. Calico

```
curl https://docs.projectcalico.org/manifests/calico.yaml -O
kubectl apply -f calico.yaml
```

---

## Other Topics

### JSON PATH

Its a query language

```
[
 "car",
 "bus",
 "truck",
 "bike"
]

$[0] # Get the 1st element
$[0,3] # Get the 1st and 4th element

```

```
{
  "car": {
    "color": "blue",
    "price": "$20,000",
    "wheels": [
      {
        "model": "X423ERT",
        "location": "front-right"
      },
      {
        "model": "X42113AD",
        "location": "front-left"
      },
      {
        "model": "X232ASD",
        "location": "rear-right"
      }
    ]
  }
}


$.car.wheels[1].model # Get the model of the 2nd wheel
$.car.wheels[?($.location == 'rear-right')].model # Get the model with rear right wheel
```

```
[
12,
43,
23,
12,
56,
43,
93,
32,
45,
63,
27,
8,
78
]

$[?(@>40)] # Get all numbers greater than 40
```

#### Wild Cars

```
{
  "car": {
    "color" : "blue",
    "price" : "$20,000"
  },
  "car": {
    "color" : "white",
    "price" : "$120,000"
  }
}


$.*.color # Get all colors
$.*.price # Get all prices
```

```
{
  "car": {
    "color": "blue",
    "price": "$20,000",
    "wheels": [
      {
        "model": "X423ERT",
        "location": "front-right"
      },
      {
        "model": "X42113AD",
        "location": "front-left"
      },
      {
        "model": "X232ASD",
        "location": "rear-right"
      }
    ]
  }
}


$[*].model # Get all wheel's model
```

#### Lists

```
[
  "Apple",
  "Google",
  "Microsoft",
  "Amazon",
  "Facebook",
  "Coca-Cola",
  "Samsung",
  "Disney",
  "Toyota",
  "McDonalds"
]


$[0:3] # Get the 1st to 4th element
$[0:8:2] # Start:End:Step
$[-1:] # Last item in the list
$[-1:0] # Last item in the list
$[-3:] # Get the last 3 elements
```

#### JSON PATH WITH KUBERNETES

https://kodekloud.com/topic/labs-json-path-kubernetes/

```
kubectl get nodes -o json
kubectl get pods -o-jsonpath='{.items[0].spec.containers[0].image}'
kubectl get nodes -o=jsonpath='{.items[*].metadata.name} {"\n"} {.items[*].status.capacity.cpu}'
kubectl get nodes -o=custom-columns=NODE:.metadata.name, CPU:.status.capacity.cpu
kubectl get nodes --sort-by=.metadata.name
```
